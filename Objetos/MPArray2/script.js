const carros = ["ford", "chevrolet", "fiat"];

carros.forEach((itens, index, array) => {
  // console.log(itens, index, array);
});

const li = document.querySelectorAll("li");

const returnForEach = li.forEach((li) => li.classList.add("ativo"));
const returnMap = Array.from(li).map((li) => {
  li.classList.add("ativado");
  return li;
});
li.forEach(function (li) {
  li.classList.add("ativado");
});
// console.log(returnForEach, returnMap);

const aulas = [
  {
    nome: "Javascript",
    horas: 20,
  },
  {
    nome: "Python",
    horas: 40,
  },
  {
    nome: "React",
    horas: 30,
  },
  {
    nome: "Angular",
    horas: 50,
  },
];

const tempoAulas = aulas.map((aula) => aula.horas);
const tempoTot = tempoAulas.reduce(
  (prevValue, curValue) => prevValue + curValue,
  0
);
// console.log(tempoAulas); // [20, 40, 30, 50]
// console.log(tempoTot); // 140

const maiorQue = tempoAulas.reduce(
  (prevValue, curValue) => (prevValue > curValue ? prevValue : curValue),
  0
);

// console.log(aulas)

const listaAulass = aulas.reduce((acumulador, aula, index) => {
  acumulador[index] = aula.nome;
  return acumulador;
}, {});

// console.log(listaAulass)

const listaRoupas = [
  "camisa",
  "calça",
  false,
  "calça moletom",
  0,
  "",
  "bermuda",
  "blusa",
  undefined,
];
const some = listaRoupas.some((item) => item === "calça");
const every = listaRoupas.every((item) => item);
const find = listaRoupas.find((item) => item === "calça");
const findIndex = listaRoupas.findIndex((item) => item === "bermuda");
const filter = listaRoupas.filter((item) => item);
const maior35 = aulas.filter((aula) => aula.horas > 35);

// console.log(some)
// console.log(every)
// console.log(find)
// console.log(findIndex)
// console.log(filter)
// console.log(maior35)

// Selecione cada curso e retorne uma array
// com objetos contendo o título, descricao,
// aulas e horas de cada curso

// Retorne uma lista com os
// números maiores que 100
const numeros = [3, 44, 333, 23, 122, 322, 33];
const maior100 = numeros.filter((numb) => numb > 100);
// console.log(maior100);

// Verifique se Baixo faz parte
// da lista de instrumentos e retorne true
const instrumentos = ["Guitarra", "Baixo", "Bateria", "Teclado"];

const baixo = instrumentos.some((item) => item === "Baixo");
// console.log(baixo);

// Retorne o valor total das compras
const compras = [
  {
    item: "Banana",
    preco: "R$ 4,99",
  },
  {
    item: "Ovo",
    preco: "R$ 2,99",
  },
  {
    item: "Carne",
    preco: "R$ 25,49",
  },
  {
    item: "Refrigerante",
    preco: "R$ 5,35",
  },
  {
    item: "Quejo",
    preco: "R$ 10,60",
  },
];

const valorTot = compras.reduce((prevValue, item) => {
  const valorLimpo = +item.preco.replace("R$ ", "").replace(",", ".");
  return prevValue + valorLimpo;
}, 0);

console.log(valorTot)