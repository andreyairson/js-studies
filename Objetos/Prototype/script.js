//* Prototype

function Personal(name, age, nationality) {
  this.name = name;
  this.age = age;
  this.nationality = nationality;
  return;
}

Personal.prototype.andar = function () {
  return `${this.name} andou bastante!`;
};

const andrey = new Personal("Andrey", 25, "brasileiro");

// console.log(andrey.andar())
// console.log(Personal.prototype)

// Crie uma função construtora de Pessoas
// Deve conter nome, sobrenome e idade
// Crie um método no protótipo que retorne
// o nome completo da pessoa

function Pessoas(nome, sobrenome, idade) {
  this.nome = nome;
  this.sobrenome = sobrenome;
  this.idade = idade;
  return;
}

Pessoas.prototype.nomeCompleto = function () {
  return `${this.nome} ${this.sobrenome}`;
};

const pessoa = new Pessoas("Andrey", "Borges", 25);

// console.log(pessoa.nomeCompleto())

// Liste os métodos acessados por
// dados criados com NodeList,
// HTMLCollection, Document

// console.log(Object.getOwnPropertyNames(NodeList.prototype))
// console.log(Object.getOwnPropertyNames(HTMLCollection.prototype))
// console.log(Object.getOwnPropertyNames(Document.prototype))

// Liste os construtores dos dados abaixo
const li = document.querySelector("li");

li; //* HTMLLIElement
li.click; //* Function
li.innerText; //* String
li.value; //* Number
li.hidden; //* Boolean
li.offsetLeft; //* Number
li.click(); //* Undefined
// Qual o construtor do dado abaixo:
li.hidden.constructor.name; //* String
console.log(li.hidden.constructor.name);

const lis = document.querySelectorAll("li");
const listaArray = [... document.querySelectorAll("li")];
const listaArray2 = Array.from(lis)
const listaArray3 = Array.prototype.slice.call(lis)

console.log(listaArray,listaArray2,listaArray3)
