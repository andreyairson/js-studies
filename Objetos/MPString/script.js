const comida = "pizza";
const agua = new String("agua");
const ano = new String("2022");

// console.log(comida, agua, ano);
// console.log(comida[comida.length - 1], agua[--agua.length], ano[ano.length - 1]);
// console.log(comida.charAt(0), agua.charAt(0), ano.charAt(0));

const frase = "Aprendendo a melhor maneira de usar ";
const frase2 = "o JavaScript";

// console.log(frase + frase2);
// console.log(frase.concat(frase2));

const fruta = "banana";
const listaFrutas = "maçã, banana, laranja";
//* O includes ele procura exatamente a forma que está, por exemplo. */
// console.log(listaFrutas.includes(fruta)); // true
// console.log(listaFrutas.includes('Banana')); // false
// console.log(listaFrutas.includes(fruta, 6));

fruta.startsWith("ba"); // true
fruta.startsWith("Ba"); // false
fruta.endsWith("na"); // true

listaFrutas.slice(7, 9); // an
listaFrutas.substring(7, 9); // an
listaFrutas.indexOf("j"); // 19
listaFrutas.lastIndexOf("a"); // 20
// console.log(listaFrutas);

const listaPreços = [
  "R$145.99",
  "R$202356.99",
  "R$3155.99",
  "R$44.99",
  "R$5.99",
];
listaPreços.forEach((item) => {
  // console.log(item.padStart(15, '.'));
  // console.log(item.padEnd(15, '.'));
});

let listaMarcas = "UnderArmor Jordan Nike Adidas Puma Lacoste";
const listaAtt = listaMarcas.replace(/[ ]+/g, ", ");
const ArrayLista = listaAtt.split(", ");

// console.log(listaMarcas.toUpperCase());
// console.log(listaMarcas.toLowerCase());

const Andrey = "    Andrey Borges         ";
// console.log();
// console.log(Andrey.trim(),Andrey.trimStart(),Andrey.trimEnd(),Andrey.trim())
// console.log()

//* Exercicios /

// Utilizando o foreach na array abaixo,
// some os valores de Taxa e os valores de Recebimento

const transacoes = [
  {
    descricao: "Taxa do Pão",
    valor: "R$ 39",
  },
  {
    descricao: "Taxa do Mercado",
    valor: "R$ 129",
  },
  {
    descricao: "Recebimento de Cliente",
    valor: "R$ 99",
  },
  {
    descricao: "Taxa do Banco",
    valor: "R$ 129",
  },
  {
    descricao: "Recebimento de Cliente",
    valor: "R$ 49",
  },
];

let valorTaxas = 0;
let valorReceb = 0;
transacoes.forEach((item) => {
  const taxasEditadas = +item.valor.replace("R$", "");
  if (item.descricao.includes("Taxa")) valorTaxas += taxasEditadas;
  if (item.descricao.includes("Recebimento")) valorReceb += taxasEditadas;
});
const valorTotal = `Valor total dos dados recebidos é: R$ ${valorTaxas - valorReceb}`;
console.log(valorTotal);
// Retorne uma array com a lista abaixo
const transportes = "Carro;Avião;Trem;Ônibus;Bicicleta";
const arrayTransportes = transportes.split(";");
// console.log(arrayTransportes);

// Substitua todos os span's por a's
const html = `<ul>
                <li><span>Sobre</span></li>
                <li><span>Produtos</span></li>
                <li><span>Contato</span></li>
              </ul>`;
const html2 = html.replace(/<span>/g, "<a>");
const html3 = html.split("span").join('a');
// console.log(html2, html3);
// Retorne o último caracter da frase
const frase3 = "Melhor do ano!";
// console.log(frase3.charAt(--frase3.length));
// console.log(frase3[--frase3.length]);

// Retorne o total de taxas
const transacoes2 = [
  "Taxa do Banco",
  "   TAXA DO PÃO",
  "  taxa do mercado",
  "depósito Bancário",
  "TARIFA especial",
];

let totTransacoes = 0;

transacoes2.forEach((item) => {
  const taxasEditadas = item.trim().toLocaleLowerCase();
  if (taxasEditadas.includes("taxa")) totTransacoes++;
})

console.log(totTransacoes)