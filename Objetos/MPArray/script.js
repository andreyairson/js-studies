const instrumentos = ["violão", "guitarra", "contra-baixo",'acordeon','violino'];
const precos = [5000, 2000, 1000, 500];
const dados = [
  new String("tipo 1"),
  ["carros", "motos", { cor: "azul", marca: "fiat" }],
  function andar(nome) {
    console.log(nome);
  },
];

const objeto = {
  0: 'tipo 1',
  1: 'tipo 2',
  2: 'tipo 3',
  3: 'tipo 4',
  4: 'tipo 5',
  length: 5 // precisa ter o length para poder transformar em array o objeto.
}

const arrayObj = Array.from(objeto)

const li = document.querySelectorAll("li");
const Newli = Array.from(li)
// console.log(Newli);
// console.log(arrayObj);
// console.log(Array.isArray(li)); // false
// console.log(Array.isArray(Newli)); // true
// console.log(Array.isArray(objeto)); // false
// console.log(Array.isArray(arrayObj)); // true
// console.log(Array.of(10, 20, 30)); // [10, 20, 30]
// console.log(new Array(5)); // [, , , , ]
// console.log(Array(5)); // [, , , , ]
// console.log(Array(5,3,6,1,26)); // [5,3,6,1,26]

const instrumentosOrdenados = instrumentos.sort();
// console.log(instrumentosOrdenados); // utilizado para ordenar strings de nomes, para numeros não fucinona muito bem!!

// const carros = ["BMW", "Mercedes", "Audi", "Volkswagen"];
// carros.unshift('Ferrari', 'Lamborghini');
// const ListaCompletaCarros = carros.push('Bugatti', 'Porsche', 'Lancia');
// const carros2 = carros
// console.log(carros); // Retorna a array com os carros adicionados
// console.log(ListaCompletaCarros); // Retorna o tamanho da array
// console.log(carros.pop()); // retira o ultimo elemento da array e toma para si o valor
// console.log(carros);
// console.log(carros.shift()); // retira o primeiro elemento da array e toma para si o valor
// console.log(carros);
// console.log(carros.reverse()); // inverte a ordem da array e modifica a array para este estado
// console.log(carros.splice(4, 0, 'Dodge')); // adiciona um elemento na posição 4 e retorna a array com o elemento adicionado
// console.log(carros.splice(2, 5, 'Ford')); // adiciona um elemento na posição 2, remove 5 elementos, que serão capturados, caso queria usalos em uma constante e retorna a array com o elemento adicionado e os elementos removidos!
// console.log(carros.copyWithin(2,0,2)); // copia os elementos da posição 0 até a posição 2 e coloca na posição 2
// console.log(carros.fill('Ford', 2)); // preenche os elementos da posição 2 até o fim com o valor Ford'

const transporte = ['carro', 'moto', 'caminhao', 'aviao', 'barco'];
const transporte2 = ['onibus', 'van', 'camelo','carro'];
const trasportes = transporte.concat(transporte2)
// console.log(trasportes,transporte,transporte2);
// console.log(trasportes.includes('carro')); // verifica se existe um elemento na array
// console.log(trasportes.indexOf('barco')); // retorna a posição do primeiro elemento que ele encontrar array que seja igual ao elemento passado
// console.log(trasportes.lastIndexOf('carro')); // retorna a posição do ultimo elemento que ele encontrar array que seja igual ao elemento passado
// console.log(trasportes.slice(3,8)); // retorna um array com os elementos da posição 3 até a posição 8
// console.log(trasportes)
// let htmlString = '<h2>Andrey Borges</h2>'
// htmlString = htmlString.split('h2')
// htmlString = htmlString.join('a')
// console.log(htmlString);

//* Exercicios /
const comidas = ['Pizza', 'Frango', 'Carne', 'Macarrão'];

// Remova o primeiro valor de comidas e coloque em uma variável
const pizza = comidas.shift()
// Remova o último valor de comidas e coloque em uma variável
const macarrao = comidas.pop()
// Adicione 'Arroz' ao final da array
comidas.push('Arroz')
// Adicione 'Peixe' e 'Batata' ao início da array
comidas.unshift('Peixe','Batata')

const estudantes = ['Marcio', 'Brenda', 'Joana', 'Kleber', 'Julia'];
// Arrume os estudantes em ordem alfabética
const ordemAlfabetica = estudantes.sort()
// Inverta a ordem dos estudantes
const ordemRevertida = estudantes.slice();
ordemRevertida.reverse();
// Verifique se Joana faz parte dos estudantes
// Verifique se Juliana faz parte dos estudantes
estudantes.includes('Joana')
estudantes.includes('Juliana')

console.log(estudantes.includes('Joana'), estudantes.includes('Juliana'), ordemAlfabetica, ordemRevertida);

//
let html = `<section>
              <div>Sobre</div>
              <div>Produtos</div>
              <div>Contato</div>
            </section>`
// Substitua section por ul e div com li,
// utilizando split e join
html = html.split('section').join('ul').split('div').join('li')
console.log(html);

const carro = ['Ford', 'Fiat', 'VW', 'Honda'];
const carrosOriginal = carro.slice()
carro.pop()
// Remova o último carro, mas antes de remover
// salve a array original em outra variável
console.log(carro, carrosOriginal)

