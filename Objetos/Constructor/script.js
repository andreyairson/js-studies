// * Constructor Function

function Carro(marca, modelo, ano, preco) {
  const taxa = 1.8;
  const precofinal = preco * taxa;
  this.marca = marca;
  this.modelo = modelo;
  this.ano = ano;
  this.preco = precofinal;
}

const honda = new Carro("honda", "civic", 2017, 20000);
const fiat = new Carro("fiat", "uno", 2017, 10000);

// console.log(honda,fiat,Carro);

// const Dom = {
//   seletor: "li",
//   element() {
//     return document.querySelector(this.seletor);
//   },
//   actived() {
//     return this.element().classList.add("active");
//   },
// };

function Dom(selec) {
  this.element = function () {
    return document.querySelector(selec);
  };
  this.actived = function () {
    return this.element().classList.add("active");
  };
}

const li = new Dom("li");
// console.log(li.element(), li.actived());

// * Exercicio
// Transforme o objeto abaixo em uma Constructor Function
function Personal(name, age) {
  this.nome = name;
  this.idade = `${age} anos`;
  this.andar = function () {
    return `${name} andou`;
  };
}

// Crie 3 pessoas, João - 20 anos,
// Maria - 25 anos, Bruno - 15 anos
const pessoa = new Personal("João", 20);
const pessoa2 = new Personal("Maria", 25);
const pessoa3 = new Personal("Bruno", 15);
// console.log(pessoa, pessoa2, pessoa3);
// console.log(pessoa.andar(), pessoa2.andar(), pessoa3.andar());


// Crie uma Constructor Function (Dom) para manipulação
// de listas de elementos do dom. Deve conter as seguintes
// propriedades e métodos:
function ListDom(selec) {
  this.element = function () {
    return document.querySelectorAll(selec);
  };
  this.AddClass = function (classe) {
    return this.element().
    forEach((item) => item.classList.add(classe));
  };
  this.RemoveClass = function (classe) {
    return this.element().
    forEach(item => item.classList.remove(classe));
  };
}

// elements, retorna NodeList com os elementos selecionados
// addClass(classe), adiciona a classe a todos os elementos
// removeClass(classe), remove a classe a todos os elementos


const allList = new ListDom("li");
allList.AddClass("active");
console.log(allList.element());
allList.RemoveClass("active");
console.log(allList.element());
