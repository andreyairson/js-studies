const perimetro = new Function("lado", "return lado * 4");

function somar({ itn1, itn2 }) {
  const somaTot = itn1 + itn2;
  return somaTot;
}

// console.log(somar({ itn1: 5, itn2: 7 }));
// console.log(somar.length);
// console.log(somar.name);
// console.log();

function Dom(seletor) {
  this.element = document.querySelector(seletor);
}

Dom.prototype.active = function (ClassName) {
  this.element.classList.add(ClassName);
};
const ul = new Dom("ul");
const li = {
  element: document.querySelector("li"),
};
const li2 = {
  element: document.querySelector("li:nth-child(2)"),
};

Dom.prototype.active.call(li, "Lacazette");
Dom.prototype.active.call(li2, "LacazettedePapel");
// ul.active('Louco')
// ul.active.call(li, 'Louco')
// console.log(ul)
// console.log(ul.element)
// console.log(li)

const frutas = ["Banana", "Maçã", "Uva", "Morango", "Abacaxi", "Melancia"];
Array.prototype.pop.call(frutas);
frutas.pop();
// console.log(frutas)

const lis = document.querySelectorAll("li");
const fillter = Array.prototype.filter.call(lis, (item) =>
  item.classList.contains("Lacazette")
);
const fillter2 = Array.prototype.filter.bind(lis, (item) =>
  item.classList.contains("LacazettedePapel")
);

// console.log(fillter,fillter2)
// console.log(fillter2())

const arrayNumeros = [1, 2, 265223, 411, 525, 426, 76, 28, 119, 32310];
// console.log(Math.max(...arrayNumeros)) // Variação
// console.log(Math.max.apply(null,arrayNumeros)) // Precisa de uma array para funcionar

const carro = {
  ano: 2019,
  marca: "Ford",
  acelerar: function (acelerar, tempo) {
    return `${this.marca} acelerou ${acelerar}km/h em ${tempo} segundos`;
  },
};

const honda = {
  marca: "Honda",
};

const aceleraHonda = carro.acelerar.bind(honda);

// console.log(aceleraHonda(200,10));

function imc(altura, peso) {
  return peso / (altura * altura);
}

const somente180 = imc.bind(null, 1.8);

// console.log(imc(1.80,70))
// console.log(somente180(120))

// * Exercicios /

// Retorne a soma total de caracteres dos
// parágrafos acima utilizando reduce
const allp = document.querySelectorAll(".this p");
const allpArray = [...allp];
console.log(allpArray);
const totAllp = allpArray.reduce((itemAtual, proximoItem) => {
  return itemAtual + proximoItem.innerText.length 
}, 0);
console.log(totAllp);

// Crie uma função que retorne novos elementos
// html, com os seguintes parâmetros
// tag, classe e conteudo.

function CriarElement(Tag,Classe,Content){
  const element = document.createElement(Tag);
  Classe ? element.classList.add(Classe): null;
  Content ? element.innerText = Content : null;
  return element;
}
console.log(CriarElement('section','active','sou o mais brabo'))



// Crie uma nova função utilizando a anterior como base
// essa nova função deverá sempre criar h1 com a
// classe titulo. Porém o parâmetro conteudo continuará dinâmico.

const newElement = CriarElement.bind(null,'h1','titulo')

console.log(newElement('sou o mais brabo'))

