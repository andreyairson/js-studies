//* Metodos do Number/
const num = Number;
// console.log(num.isNaN(NaN)); // true
// console.log(num.isInteger(2022)); // true
// console.log(num.isInteger(24.9)); // false
// console.log(num.parseFloat("24.9")); // 24.9 number
// console.log(num.parseFloat("Vinte quatro ponto nove 24.9")); // NaN
// console.log(num.parseInt("24.9 vinte quatro ponto nove")); // 24 number
// console.log(num.parseInt(24.999999, 10)); // 24

// Metodos de numeros
const ano = 2022.8546;
const preco = 200.45;
const ano2 = +"2022.8546";
const anoAtual = new Number("2022");

// console.log(ano.toFixed()); // 2023
// console.log(ano.toFixed(2)); // 2022.85
// console.log(ano.toString(10)); // string
// console.log(
//   preco.toLocaleString("pt-br", { style: "currency", currency: "BRL" }) // R$200,45
// ); 
// console.log(
//   preco.toLocaleString("en-us", { style: "currency", currency: "USD" }) // $200.45
// ); 
// console.log(Math.abs(-5.5)) // transforma em positivo 5.5
// console.log(Math.ceil(5.223)) // arredonda para cima 6
// console.log(Math.floor(5.97)) // arredonda para baixo 5
// console.log(Math.round(5.6)) // arredonda somente 6 
// console.log(Math.round(5.4)) // arredonda somente 5
// console.log(Math.max(5,4,8,45,66,2)) // 66
// console.log(Math.min(5,4,8,45,66,2)) // 2
// console.log(Math.random()) // gera um numero aleatorio
// console.log(+Math.random().toFixed(4) * 10) // gera um numero aleatorio


//* Exercicios /

// Retorne um número aleatório
// entre 1050 e 2000
const number = Math.floor(Math.random() * (2000 - 1050 + 1)) + 1050;
// console.log(number);


// Retorne o maior número da lista abaixo
const numeros = '4, 5, 20, 8, 9';
const numerosArray = numeros.split(', ');
const MaiorNumero2 = Math.max(...numerosArray)
let MaiorNumero = 0
numerosArray.forEach((numb)=>{
  MaiorNumero = Math.max(MaiorNumero, numb)
})
// console.log(MaiorNumero)
// console.log(MaiorNumero2)


// Crie uma função para limpar os preços
// e retornar os números com centavos arredondados
// depois retorne a soma total
const listaPrecos = ['R$ 59,99', ' R$ 100,222',
                     'R$ 230  ', 'r$  200'];



const limparPreco = (preco) => {
  preco = preco = +preco.toUpperCase().replace('R$ ', '').trim().replace(',','.')
  preco = +preco.toFixed(2)

  return preco
}

let soma = 0

listaPrecos.forEach((preco)=>{
  soma += limparPreco(preco)
})

soma = soma.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
console.log(soma) // R$ 590,21


