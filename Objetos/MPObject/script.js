const pessoa = new Object({
  nome: "Andrey",
});
// console.log(pessoa)

const carro = {
  rodas: 4,
  init(valor) {
    this.marca = valor;
    return this;
  },
  acelerar() {
    return `${this.marca} fez vrummmm`;
  },
  buzinar() {
    return `${this.marca} fez beep`;
  },
};

const honda = Object.create(carro).init("Honda");
const lamburguini = Object.create(carro).init("lamburguini");
// console.log(honda.buzinar())
// console.log(lamburguini.acelerar())

const funcaoAutomovel = {
  acelerar() {
    return `${this.marca} fez vrummmm`;
  },
  buzinar() {
    return `${this.marca} fez UM SAI CARAI`;
  },
};

const moto = {
  rodas: 2,
  capacete: true,
};

const carro2 = {
  rodas:4,
  mala: true,
};

// console.log(carro2)
// console.log(moto)

Object.assign(moto,funcaoAutomovel)
Object.assign(carro2,funcaoAutomovel)

// console.log(carro2)
// console.log(moto)

// Object.defineProperties(moto,{
//   rodas:{
//     writable:false, // impede a mudança do valor 
//     configurable:false, // impede a exclusão do atributo | as 3 ja vem false por padrão.
//     enumerable:false, // permite a listagem do atributo
//   }
// })

// delete moto.rodas

const moto2 = {}
Object.defineProperties(moto2, {
  velocidade: {
    get() {
      return this._velocidade;
    },
    set(valor) {
      this._velocidade = 'Velocidade ' + valor;
    }
  },
})
moto2.velocidade = 200;
moto2.velocidade; // Velocidade 200

// console.log(moto2)

// Object.getOwnPropertyDescriptors(Array);
// // Lista com métodos e propriedades e Array

// Object.getOwnPropertyDescriptors(Array.prototype);
// // Lista com métodos e propriedades do protótipo de Array

// Object.getOwnPropertyDescriptor(window, 'innerHeight');
// // Puxa de uma única propriedade


// Object.keys(Array);
// // [] vazia, pois não possui propriedades enumeráveis

// const carro = {
//   marca: 'Ford',
//   ano: 2018,
// }
// Object.keys(carro);
// // ['marca', 'ano']
// Object.values(carro);
// // ['Ford', 2018]
// Object.entries(carro);
// // [['marca', 'Ford'], ['ano', 2018]]

// Object.getOwnPropertyNames(Array);
// // ['length', 'name', 'prototype', 'isArray', 'from', 'of']

// Object.getOwnPropertyNames(Array.prototype);
// // [..., 'filter', 'map', 'every', 'some', 'reduce', ...]

// const carro = {
//   marca: 'Ford',
//   ano: 2018,
// }
// Object.getOwnPropertyNames(carro);
// // ['marca', 'ano']

// const frutas = ['Banana', 'Pêra']
// Object.getPrototypeOf(frutas);
// Object.getPrototypeOf(''); // String

// const frutas1 = ['Banana', 'Pêra'];
// const frutas2 = ['Banana', 'Pêra'];

// Object.is(frutas1, frutas2); // false



// const carro = {
//   marca: 'Ford',
//   ano: 2018,
// }
// Object.freeze(carro);
// Object.seal(carro);
// Object.preventExtensions(carro);

// Object.isFrozen(carro); // true
// Object.isSealed(carro); // true
// Object.isExtensible(carro); // False


// const frutas = ['Banana', 'Uva'];
// frutas.constructor; // Array

// const frase = 'Isso é uma String';
// frase.constructor; // 



// const frutas = ['Banana', 'Uva'];

// frutas.hasOwnProperty('map'); // false
// Array.hasOwnProperty('map'); // false
// Array.prototype.hasOwnProperty('map'); // true

// Array.prototype.propertyIsEnumerable('map'); // false
// window.propertyIsEnumerable('innerHeight'); // true

// const frutas = ['Banana', 'Uva'];

// Array.prototype.isPrototypeOf(frutas); // 


// const frutas = ['Banana', 'Uva'];
// frutas.toString(); // 'Banana,Uva'
// typeof frutas; // object
// Object.prototype.toString.call(frutas); // [object Array]

// const frase = 'Uma String';
// frase.toString(); // 'Uma String'
// typeof frase; // string
// Object.prototype.toString.call(frase); // [object String]

// const carro = {marca: 'Ford'};
// carro.toString(); // [object Object]
// typeof carro; // object
// Object.prototype.toString.call(carro); // [object Object]

// const li = document.querySelectorAll('li');
// typeof li; // object
// Object.prototype.toString.call(li); // [object NodeList]

// * Exercicios * \\

// Crie uma função que verifique
// corretamente o tipo de dado

function verify (type, data){
  return type.prototype(data)

}

// Crie um objeto quadrado com
// a propriedade lados e torne
// ela imutável
const quadrado = {
  lados: 4
}

Object.defineProperties(quadrado, {
  lados: {
    writable: false,
  }
})

quadrado.lados = 5
console.log(quadrado)


// Previna qualquer mudança
// no objeto abaixo
const configuracao = {
  width: 800,
  height: 600,
  background: '#333'
}

Object.freeze(configuracao);

// Liste o nome de todas
// as propriedades do
// protótipo de String e Array

const string = 'Isso é uma String';

console.log(Object.getOwnPropertyNames(string.__proto__))
console.log(Object.getOwnPropertyNames(String.prototype))
console.log(Object.getOwnPropertyNames(Array.prototype))
