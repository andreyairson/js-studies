/* Events */

// const actived = document.querySelector('.actived');

// function buttonActived() {
//   console.log('Click on: ',actived);
// }

// /* The passed function is called Callback*/

// actived.addEventListener('click', buttonActived);

// console.log(actived);

// const actived = document.querySelector('.actived');
// /**  @param {MouseEvent} event */
// function buttonActived(event){
//   console.log(event.timeStamp)
// }

// actived.addEventListener('click', buttonActived);

// console.log(actived);

/* CODIGO IMPORTANTE PARA TIRAR E COLOCAR CLASS */

// const button = document.querySelector('.button');

// function buttonActived(){
//   const text = document.querySelector('.text')
//   text.classList.toggle('actived')
// }

// button.addEventListener('click', buttonActived);

// const screenWidth = window.innerWidth

// console.log(screenWidth)

// function mouseFollow (event){
//   // const x = event.x;
//   // const y = event.y;

//   const cord = {
//     x:"está " + event.x,
//     y:"está " + event.y,
//   }

//   console.log(cord);
// }

// window.addEventListener('mousemove', mouseFollow)

// function mouseScroll(){
//   console.log(window.scrollY)

// }

// window.addEventListener("scroll",mouseScroll)

const div = document.querySelector("div");

function handleClick() {

  div.classList.toggle("teucu")

}

window.addEventListener("click", handleClick);

function handleMouseMove(event){
   // div.style.marginTop = `calc(${event.clientY}px - 12px)`;
  // div.style.marginLeft = `calc(${event.clientX}px - 12px)`;

  // div.style.marginLeft = "calc(" + event.clientX + "px - 12px)";
  // div.style.marginTop = "calc(" + event.clientY + "px - 12px)";

  // div.style.marginLeft = event.clientX - 12 + "px";
  // div.style.marginTop = event.clientY - 12 + "px";

  div.style.marginLeft = event.clientX - 12 + "px";
  div.style.marginTop = event.clientY - 12 + "px";


  // div.style.width = "50px";
  // div.style.height = "50px";
  // div.style.backgroundColor = "#c99";
  // div.style.marginLeft = event.clientX - 25 + "px";
  // div.style.marginTop = event.clientY  - 25 + "px";

}

window.addEventListener("mousemove", handleMouseMove);
