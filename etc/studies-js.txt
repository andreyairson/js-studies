Adicionar JavaScript - A ordem importa, geralmente colocamos antes do fechamento da tag </body>

ex.:
<body>
<script>
  console.log('Funciona');
</script>
</body>
<script src="./js/script.js"></script>

--

Variáveis - Guardam um valor na memória e permitem a reutilização do mesmo. Três formas de declarar const, let e var. Usamos mais const e let.

console.log() - Permite debugarmos o JavaScript, mostrando o valor passado entre os () no Console do browser.


________

const texto = document.querySelector('#texto');

console.log(texto);
console.dir(texto);

// Puxar informação
console.log(texto.innerHTML); retorna o conteudo da var que foi utulizada
console.log(texto.href); retorna o valor o href da var que foi selecionada
console.log(texto.innerText); 
console.log(texto.clientHeight);
console.log(texto.clientWidth);
texto.remove(); remove totalmente o conteudo da var selecionada

// Definir informação
texto.style.backgroundColor = 'black';
texto.style.padding = '1rem';
texto.style.height = document.body.clientHeight / 2 + 'px';

texto.classList.add('ativo');
texto.setAttribute('aria-label', 'Cursos');


---

Tipos de Dados - Existem diferentes tipos de dados no JavaScript. 
Os mais comuns são:

    String - A String guarda dados em formato de texto, geralmente colocada entre aspas simples '' ou duplas "". É possível concatenar (juntar) Strings com o sinal 
        const texto = 'Curso de JavaScript';
        const condicao = 'Novo';
        const frase = texto + ': ' + condicao;

        console.log(frase);

    Number - O Number guarda número inteiros 10 ou decimais 10.5 // float int
    ex.:
        let total = 0;

        const compras = 30;
        const imposto = 10;

        total = compras + imposto;
        console.log(total);

        const dobro = total * 2;
        const metade = total / 2;
        const desconto = total - 20;

        const strings = '20' + '20'; // 2020
        const numbers = 20 + 20; // 40 
        
        const transformar = Number("20") + 20; irá transformar a string em number, pois está utilizando a função number para realizar essa trasnformação.


    Object - Um objeto (Object) é uma forma de matermos dados associados e organizados. Ele é criado com chaves {}. "Tudo é objeto", vocês já vão ver o motivo.
    ex.:
        const nome = 'O Senhor dos Anéis';
        const ano = 1954;
        const autor = 'J. R. R. Tolkien';

        const nomeFilme = 'O Senhor dos Anéis';
        const anoFilme = 2001;
        const diretorFilme = 'Peter Jackson';

        const livro = {
        nome: 'O Senhor dos Anéis',
        ano: 1954,
        autor: 'J. R. R. Tolkien',
        };

        const filme = {int-float - valores inteiros
        nome: 'O Senhor dos Anéis',
        ano: 2001,
        diretor: 'Peter Jackson',
        };

        console.log(livro);
        console.log(livro.nome);
        console.log(livro.ano);

        console.log(filme);
        console.log(filme.diretor);


    Array

    Boolean

    Null/Undefined
    
    
    ---
    Var - guarda informação
    Argumento ou parametro - referencia a algo, ao valor que se entrega na função.
        
    ---
Funções - As funções (Function) são blocos de código que podem ser executados. A execução pode acontecer em diferentes cenários e diversas vezes. Para executar utilizamos os parênteses ();

Parâmetro e Argumento - Podemos definir parâmetros durante a criação da função. Durante a execução, podemos passar argumentos ('nome'), que serão utilizados nos parâmetros.

Retorno - Toda função retorna um valor (tipo de dado). Se o retorno não for declarado utilizado a palavra return, o valor retornado será undefined.

Escopo - Uma função cria o seu próprio escopo. Isso significa que variáveis declaradas dentro de uma função, não podem ser acessadas fora das mesmas. Porém, variáveis criadas fora da função, podem ser acessadas pela mesma.

ESCOPO LÉXICO - Funções conseguem acessar variáveis que foram criadas no contexto pai

PARÂMETROS E ARGUMENTOS - Ao criar uma função, você pode definir parâmetros.
Ao executar uma função, você pode passar argumentos.

PODE OU NÃO RETORNAR UM VALOR - Quando não definimos o return, ela irá retornar undefined. O código interno da função é executado normalmente, independente de existir valor de return ou não.

*ARGUMENTOS PODEM SER FUNÇÕES*
------


Métodos - Um método é uma função dentro de um objeto.

Eventos - Podemos adicionar funções que serão executadas caso um evento ocorra. O evento ocorre no documento inteiro ou no elemento do dom. Usamos o método addEventListener para adicionar eventos.
ex.:
texto.classList.add('ativo');       - adiciona uma class do html
texto.classList.remove('ativo');    - remove uma class do html
texto.classList.toggle('ativo');    - adiciona e remove uma class no html


Event - A função executada possui acesso ao argumento do evento. Esse argumento é um objeto com diferentes propriedades e métodos sobre o evento que ocorreu.

Interação - O poder do JavaScript está na manipulação do DOM através de eventos executados pelo usuário. Um comum seria a adição/remoção de uma classe.

window - O window é um objeto que representa a janela do browser e contém todo o dom. Esse objeto além de possuir diversas propriedades e métodos com informações sobre a página, pode também receber eventos globais.

----

Arrays - Uma Array [] representa uma lista de dados. Para retornar um valor passamos o [index] na frente da variável, o index começa em 0.

Tipos de Dados - É possível colocar qualquer tipo de dado em uma array.

Métodos e Propriedades - Uma Array possui diferentes métodos e propriedades.

ex.:
const lista = ['JavaScript', 'HTML', 'CSS'];

console.log(lista);

const js = lista[0];
const html = lista[1];
const css = lista[2];

ex2.:
const listaComplexa = [
  { nome: 'André', sobrenome: 'Rafael' },
  100,
  'Mestre',
  true,
  [20, 30],
];

ex de propriedades:
const lista = ['JavaScript', 'HTML', 'CSS'];

// retorna o total
const total = lista.length;

// remove o último
const ultimo = lista.pop();

// remove o primeiros
const primeiro = lista.shift();

// adiciona ao final
lista.push('PHP');

console.log(lista);

obs.:
Array vs Array-like
Um objeto Array-like não possui todos os métodos/propriedades de Array's, mas podemos transformar eles com a função Array.from().
html
<ul>
  <li><a href="./">Home</a></li>
  <li><a href="./produtos.html">Produtos</a></li>
  <li><a href="./contato.html">Contato</a></li>
</ul>

js

const links = Array.from(document.querySelectorAll('a'));

// erro se não usar o Array.from
links.pop();
console.log(links);

---

Loop - Um loop pode ser utilizado para executar um código para cada item da array.
ex.:

const lista = ['JavaScript', 'HTML', 'CSS'];
const body = document.querySelector('body');

for (let index = 0; index < lista.length; index++) {
  const item = lista[index];
  body.innerHTML += '<li>' + lista[index] + '</li>';
}



forEach - Arrays e objetos que se parecem com Array's, possuem o método forEach. Esse método recebe uma função que é executada para cada item da Array.

ex.:
const lista = ['JavaScript', 'HTML', 'CSS'];

function logarItems(item, index) {
  console.log(item, 'no index:', index);
}

lista.forEach(logarItems);

// você pode escrever a função no argumento:
lista.forEach(function (item, index) {
  console.log(item, 'no index:', index);
});


--


querySelectorAll - O método document.querySelectorAll(), seleciona todos os elementos que suprirem a condição do argumento e retorna uma array-like (objeto que se parece com uma array).
ex.:
html
<ul>
  <li><a href="./">Home</a></li>
  <li><a href="./produtos.html">Produtos</a></li>
  <li><a href="./contato.html">Contato</a></li>
</ul>

js
const links = document.querySelectorAll('a');

function logHref(item) {
  const href = item.href;
  console.log(href);
}

links.forEach(logHref);

// Primeiro link
console.log(links[0]);

--


