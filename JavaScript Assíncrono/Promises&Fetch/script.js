// const doc = fetch("./text.txt");

// doc
// .then(r => r.text())
// .then(body => {
//   const conteudo = document.querySelector('div')
//   conteudo.innerText = body
// });

// const cep = fetch('https://viacep.com.br/ws/01001000/json/');

// cep
// .then(r => r.json())
// .then(body => {
//   const conteudo = document.querySelector('div')
//   conteudo.innerText = body
// });

// const styleCss = fetch('./style.css');

// styleCss
// .then(r => r.text())
// .then(body => {
//   console.log(body)
//   const conteudo = document.querySelector('body')
//   const style = document.createElement('style')
//   style.innerHTML = body
//   conteudo.appendChild(style)
// });

// const body = document.querySelector('body')
// body.innerHTML = '<div><h1 class="teucu">Ola mundo</h1><h2>Isso é tudo pessoal</h2> <img src="../class12/img/imagem3.jpg" alt=""></div>'

// * Exercicios

// Utilizando a API https://viacep.com.br/ws/${CEP}/json/

// crie um formulário onde o usuário pode digitar o cep
// e o endereço completo é retornado ao clicar em buscar
// const inputCep = document.getElementById("cep");
// const btncep = document.getElementById("btnCep");
// const resultadoCep = document.querySelector(".resultadoCep");

// btncep.addEventListener("click", (event) => {
//   event.preventDefault();
//   const cep = inputCep.value;
//   buscaCep(cep);
// });

// const buscaCep = (cep) => {
//   fetch(`https://viacep.com.br/ws/${cep}/json/`)
//     .then(response => response.text())
//     .then(body => {
//       resultadoCep.innerText = body;
//     });
// };
// // Utilizando a API https://blockchain.info/ticker
// // retorne no DOM o valor de compra da bitcoin and reais.
// // atualize este valor a cada 30s

// const btc = document.querySelector('.btc')

// const fetchBtc = () =>{
//   fetch('https://blockchain.info/ticker')
//   .then(response => response.json())
//   .then(btcJason => btc.innerText = `O valor do BitCoin está em R$${btcJason.BRL.buy}`)
// }

// setInterval(fetchBtc, 1000)
// fetchBtc()

// // Utilizando a API https://api.chucknorris.io/jokes/random
// // retorne uma piada randomica do chucknorris, toda vez que
// // clicar em próxima

// const jokeContent = document.querySelector('.joke p')
// const switchJoke = document.querySelector('.joke button')

// const thisaJoke = () =>{
//   fetch('https://api.chucknorris.io/jokes/random')
//   .then(r => r.json())
//   .then(joke => jokeContent.innerText = joke.value)
// }

// switchJoke.addEventListener('click', thisaJoke)

// thisaJoke()