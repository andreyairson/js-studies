// function createButton (text){
//   // const numeroSecreto = Math.floor(Math.random() * 100);
//   const element = () =>{
//     const buttonElement = document.createElement('button');
//     buttonElement.innerText = text;
//     return buttonElement;
//   }
//   return {
//     text,
//     element,
//     // numeroSecreto
//   }
// }
// function createButton (text){
//   // const numeroSecreto = Math.floor(Math.random() * 100);
//   const element = () =>{
//     const buttonElement = document.createElement('button');
//     buttonElement.innerText = text;
//     return buttonElement;
//   }
//   return Object.freeze({
//     text,
//     element,
//     // numeroSecreto
//   })
// }

// const btnBlue = createButton('comprar')
// console.log(btnBlue)

// function Pessoa(nome) {
//   if (!(this instanceof Pessoa)) // ou (!new.target)
//     return new Pessoa(nome);
//   this.nome = nome;
// }

// Pessoa.prototype.andar = function() {
//   return `${this.nome} andou`;
// }

// const designer = Pessoa('André');

function $$(selectedElements) {
  const elements = document.querySelectorAll(selectedElements);

  function on(onEvent, callback) {
    elements.forEach(element => {
      element.addEventListener(onEvent, callback);
    });
    return this; // retornar this irá funcionar da mesma forma
  }

  function hide() {
    elements.forEach(element => {
      element.style.display = 'none';
    });
    return this;
  }

  function show() {
    elements.forEach(element => {
      element.style.display = 'initial';
    });
    return this;
  }

  function addClass(className) {
    elements.forEach(element => {
      element.classList.add(className);
    });
    return this;
  }

  function removeClass(className) {
    elements.forEach(element => {
      element.classList.add(className);
    });
    return this;
  }
  
  return Object.freeze({
    elements,
    on,
    hide,
    show,
    addClass,
    removeClass,
  });
}

const buttons = $$('button');
buttons.hide().show().addClass('ativo').removeClass('ativo');

