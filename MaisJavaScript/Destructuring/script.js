//* https://www.origamid.com/slide/javascript-completo-es6/#/0804-destructuring/8

// const carro = {
//   marca: 'Fiat',
//   ano: 2018,
//   portas: 4,
// }

// const {marca, ano} = carro;

// console.log(marca); // Fiat
// console.log(ano); // 2018
// const cliente = {
//   nome: 'Andre',
//   compras: {
//     digitais: {
//       livros: ['Livro 1', 'Livro 2'],
//       videos: ['Video JS', 'Video HTML']
//     },
//     fisicas: {
//       cadernos: ['Caderno 1']
//     }
//   }
// }

// console.log(cliente.compras.digitais.livros);
// console.log(cliente.compras.digitais.videos);

// const {livros, videos} = cliente.compras.digitais;

// console.log(livros);
// console.log(videos);

// const cliente = {
//   nome: 'Andre',
//   compras: {
//     digitais: {
//       livros: ['Livro 1', 'Livro 2'],
//       videos: ['Video JS', 'Video HTML']
//     },
//     fisicas: {
//       cadernos: ['Caderno 1']
//     }
//   }
// }

// const {fisicas, digitais, digitais: {livros, videos}} = cliente.compras;

// console.log(fisicas);
// console.log(livros);
// console.log(videos);
// console.log(digitais);

// const cliente = {
//   nome: 'Andre',
//   compras: 10,
// }

// const {nome, compras} = cliente;
// // ou
// const {nome: nomeCliente, compras: comprasCliente} = cliente;

// const cliente = {
//   nome: 'Andre',
//   compras: 10,
// }

// const {nome, compras, email = 'email@gmail.com', cpf} = cliente;
// console.log(email) // email@gmail.com
// console.log(cpf) // undefined

// const frutas = ['Banana', 'Uva', 'Morango'];

// const primeiraFruta = frutas[0];
// const segundaFruta = frutas[1];
// const terceiraFruta = frutas[2];

// // Com destructuring
// const [primeira, segunda, terceira] = frutas;

// const primeiro = 'Item 1';
// const segundo = 'Item 2';
// const terceiro = 'Item 3';
// // ou
// const [primeiro, segundo, terceiro] = ['Item 1', 'Item 2', 'Item 3']; 

// function handleKeyboard(event) {
//   console.log(event.key);
// }
// Com Destructuring
// function handleKeyboard({key}) {
//   console.log(key);
// }

// document.addEventListener('keyup', handleKeyboard);


// Extraia o backgroundColor, color e margin do btn
const btn = document.querySelector('button');
const {backgroundColor,color,margin} = getComputedStyle(btn);
console.log(backgroundColor,color,margin);

// Troque os valores das variáveis abaixo
let cursoAtivo = 'JavaScript';
let cursoInativo = 'HTML';

[cursoAtivo,cursoInativo] = [cursoInativo,cursoAtivo];

// Corrija o erro abaixo
const cachorro = {
  nome: 'Bob',
  raca: 'Labrador',
  cor: 'Amarelo'
}

const {cor: bobCor} = cachorro;
