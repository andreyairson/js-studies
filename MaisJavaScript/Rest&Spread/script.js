// function perimetroForma(lado, totalLados) {
//   return lado * totalLados;
// }

// perimetroForma(10, 4); // 40
// perimetroForma(10); // NaN


// function perimetroForma(lado, totalLados) {
//   totalLados = totalLados || 4; // se não for definido, será igual à 4
//   return lado * totalLados;
// }

// perimetroForma(10, 3); // 30
// perimetroForma(10); // 40


// function perimetroForma(lado, totalLados = 4) {
//   return lado * totalLados;
// }

// perimetroForma(10, 5); // 50
// perimetroForma(10); // 40


// function perimetroForma(lado, totalLados = 4) {
//   console.log(arguments)
//   return lado * totalLados;
// }

// perimetroForma(10);
// perimetroForma(10, 4, 20);


// function anunciarGanhadores(...ganhadores) {
//   ganhadores.forEach(ganhador => {
//     console.log(ganhador + ' ganhou.')
//   });
// }

// anunciarGanhadores('Pedro', 'Marta', 'Maria');


// function anunciarGanhadores(premio, ...ganhadores) {
//   ganhadores.forEach(ganhador => {
//     console.log(ganhador + ' ganhou um ' + premio)
//   });
// }

// anunciarGanhadores('Carro', 'Pedro', 'Marta', 'Maria');


// function anunciarGanhadores(premio, ...ganhadores) {
//   console.log(ganhadores);
//   console.log(arguments);
// }

// anunciarGanhadores('Carro', 'Pedro', 'Marta', 'Maria');


// const frutas = ['Banana', 'Uva', 'Morango'];
// const legumes = ['Cenoura', 'Batata'];

// const comidas = [...frutas, 'Pizza', ...legumes];



// const numeroMaximo = Math.max(4,5,20,10,30,2,33,5); // 33

// const listaNumeros = [1,13,21,12,55,2,3,43];
// const numeroMaximoSpread = Math.max(...listaNumeros);
// const btns = document.querySelectorAll('button');
// const btnsArray = [...btns];

// const frase = 'Isso é JavaScript';
// const fraseArray = [...frase];



// Reescreva a função utilizando
// valores iniciais de parâmetros com ES6
function createButton(background = 'blue', color = 'red') {
  const buttonElement = document.createElement('button');
  buttonElement.style.background = background;
  buttonElement.style.color = color;
  return buttonElement;
} 

const redButton = createButton();

// Utilize o método push para inserir as frutas ao final de comidas.
const frutas = ['Banana', 'Uva', 'Morango'];
const comidas = ['Pizza', 'Batata'];

comidas.push(...frutas);