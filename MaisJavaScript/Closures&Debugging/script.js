// let item1 = 1;
// function funcao1() {
//   let item2 = 2;
//   function funcao2() {
//     let item3 = 3;
//   }
// }

// func1, possui acesso à
// item1 e item2

// func2, possui acesso à
// item1, item2 e item3

// let item1 = 1;
// function funcao1() {
//   let item2 = 2;
//   function funcao2() {
//     let item3 = 3;
//     console.log(item1);
//     console.log(item2);
//     console.log(item3);
//   }
//   funcao2();
// }

// debugger; // adicione a palavra debugger
// let item1 = 1;
// function funcao1() {
//   let item2 = 2;
//   function funcao2() {
//     let item3 = 3;
//     console.log(item1);
//     console.log(item2);
//     console.log(item3);
//   }
//   funcao2();
// }

// function contagem() {
//   let total = 0;
//   return function incrementar() {
//     total++;
//     console.log(total);
//   }
// }

// const ativarIncrementar = contagem();
// ativarIncrementar(); // 1
// ativarIncrementar(); // 2
// ativarIncrementar(); // 3

// function $$(selectedElements) {
//   const elements = document.querySelectorAll(selectedElements);

//   function hide() { ... }
//   function show() { ... }
//   function on() { ... }
//   function addClass() { ... }
//   function removeClass() { ... }

//   return { hide, show, on, addClass, removeClass }
// }
