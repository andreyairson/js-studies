// function somar (a,b){
//   return a + b
// }
// console.log(somar(1,2))

// const subtrair = function (a,b){
//   return a - b
// }
// console.log(subtrair(1,2))

// Remova o erro
const priceNumber = (n) => +n.replace("R$", "").replace(",", ".");
priceNumber("R$ 99,99");
console.log(priceNumber("R$ 99,99"));

// Crie uma IIFE e isole o escopo
// de qualquer código JS.
((a,b) => {
  // const nome = "Andrey";
  // console.log(nome);
  a+b
})(console.log(5+7));
// Como podemos utilizar
// a função abaixo.
const active = (callback) => callback();
active(() => console.log("Hello World"));
