const tab = document.querySelectorAll(".jsTab li");
const tabText = document.querySelectorAll(".jsText section");
tabText[0].classList.add("active");

if (tab.length && tabText.length) {
  const activeTab = (idx) => {
    tabText.forEach((item) => {
      item.classList.remove("active");
    });
    tabText[idx].classList.add("active");
  };

  tab.forEach((item, idx) => {
    item.addEventListener("click", () => {
      activeTab(idx);
    });
  });
}
