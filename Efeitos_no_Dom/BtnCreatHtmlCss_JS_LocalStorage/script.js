const controls = document.querySelector(".controls");
const cssText = document.querySelector(".css");
const btn = document.querySelector(".btn");

const handleStyle = {
  element: btn,
  texto(value) {
    this.element.innerHTML = value;
  },
  color(value) {
    this.element.style.color = value;
  },
  backgroundColor(value) {
    this.element.style.backgroundColor = value;
  },
  height(value) {
    this.element.style.height = value + "px";
  },
  width(value) {
    this.element.style.width = value + "px";
  },
  border(value) {
    this.element.style.border = value;
  },
  borderRadius(value) {
    this.element.style.borderRadius = value + "px";
  },
  fontFamily(value) {
    this.element.style.fontFamily = value;
  },
  fontSize(value) {
    this.element.style.fontSize = value + "px";
  },
};

const handleChange = ({ target }) => {
  const name = target.name;
  const value = target.value;
  // btn.style[name] = value;
  // name === "borderRadius" || "height" || "width" || "fontSize"
  //   ? (btn.style[name] = `${value}px`)
  //   : null;
  // name === "texto" ? (btn.innerHTML = value) : null;
  handleStyle[name](value);
  showCssText();
  saveValues(name, value);
};

const showCssText = () =>
  (cssText.innerHTML = `<span> ${btn.style.cssText
    .split(";")
    .join(";</><span>")}`);

const saveValues = (name, value) => {
  localStorage[name] = value;
};
const setValues = () => {
  const properties = Object.keys(localStorage);
  properties.forEach((item) => {
    controls.elements[item].value = localStorage[item];
    handleStyle[item](localStorage[item]);
  });
  showCssText();
};

setValues();

controls.onchange = handleChange;
