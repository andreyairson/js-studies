function initTabNavegation() {
  const tab = document.querySelectorAll(".jsTab li");
  const tabText = document.querySelectorAll(".jsText section");
  tabText[0].classList.add("active");

  if (tab.length && tabText.length) {
    const activeTab = (idx) => {
      tabText.forEach((item) => {
        item.classList.remove("active");
      });
      tabText[idx].classList.add("active");
    };

    tab.forEach((item, idx) => {
      item.addEventListener("click", () => {
        activeTab(idx);
      });
    });
  }
}

function initAccordion() {
  const acordionList = document.querySelectorAll(".jsAccordion dt");
  acordionList[0].classList.add("active");
  acordionList[0].nextElementSibling.classList.add("active");
  if (acordionList.length) {
    const handleAccordion = ({ target }) => {
      // acordionList.forEach((item) => {
      //   item.classList.remove("active");
      //   item.nextElementSibling.classList.remove("active");
      // })
      target.classList.toggle("active");
      target.nextElementSibling.classList.toggle("active");
    };

    acordionList.forEach((item) => {
      item.addEventListener("click", handleAccordion);
    });
  }
}

function initScroll() {
  const linksInternos = document.querySelectorAll('a[href^="#"]');

  function scrollToSection(ev) {
    ev.preventDefault();
    const href = ev.currentTarget.getAttribute("href");
    const section = document.querySelector(href);
    section.scrollIntoView({
      behavior: "smooth",
      block: "start",
    });
    // * Scroll alternativo
    // const top = section.offsetTop;
    // window.scrollTo({
    //   top: top,
    //   behavior: "smooth",
    // });
    console.log(href, section);
  }

  linksInternos.forEach((link) =>
    link.addEventListener("click", scrollToSection)
  );
}

function initAnimationScroll() {
  const sections = document.querySelectorAll(".jsScroll");
  if (sections.length) {
    const windowHalf = window.innerHeight * 0.6;

    sections[0].classList.add("active");

    const animaScroll = () => {
      sections.forEach((section) => {
        const sectionTop = section.getBoundingClientRect().top;
        const isSectionVisible = sectionTop - windowHalf < 0;
        if (isSectionVisible) {
          section.classList.add("active");
        } else {
          section.classList.remove("active");
        }
      });
    };

    window.addEventListener("scroll", animaScroll);
  }
}

initTabNavegation();
initAccordion();
initScroll();
initAnimationScroll();
