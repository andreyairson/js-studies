import initScroll from "./module/scrollSuave.js";
import initAnimationScroll from "./module/animationScroll.js";
import initAccordion from "./module/accordion.js";
import initTabNavegation from "./module/tabNavegation.js";
import initModal from "./module/modal.js";
import initTooltip from "./module/tooltip.js";
import initDropdown from "./module/dropDown.js";
import initMenuMobile from "./module/menuMobile.js";
import initNumbers from "./module/numbers.js";
import initFuncionamento from "./module/funcionamento.js";
// import { testando, testando2 } from "./module/test.js";
// import * as teste from "./module/test.js";
// testando();
// testando2();

// console.log(teste)
// teste.testando();
// teste.testando2();
initScroll();
initAnimationScroll();
initAccordion();
initTabNavegation();
initModal()
initTooltip()
initDropdown()
initMenuMobile()
initNumbers()
initFuncionamento()