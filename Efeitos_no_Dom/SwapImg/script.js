const imagens = document.querySelectorAll("#galeria li img");

function swapImag(event) {
  const primary = document.querySelector("#img-princ");
  const clicked = event.currentTarget;
  primary.src = clicked.src;
  primary.alt = clicked.alt;
}

function loop(img) {
  img.addEventListener("click", swapImag);
}

imagens.forEach(loop);
