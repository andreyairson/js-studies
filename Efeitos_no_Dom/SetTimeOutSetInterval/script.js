// function espera(texto) {
//   console.log(texto);
// }
// setTimeout(espera, 1000, 'Depois de 1s');

// setTimeout(() => {
//   console.log('Após 0s?');
// });

// for(let i = 0; i < 20; i++) {
//   setTimeout(() => {
//     console.log(i);
//   }, 300);
// }

// for(let i = 0; i < 20; i++) {
//   setTimeout(() => {
//     console.log(i);
//   }, 300 * i);
// }

// const btn = document.querySelector('button');
// btn.addEventListener('click', handleClick);

// function handleClick(event) {
//   setTimeout(function() {
//     this.classList.add('active');
//   }, 1000)
// }
// // Erro pois window.classList não existe

// const btn = document.querySelector('button');
// btn.addEventListener('click', handleClick);

// // this agora é btn.
// function handleClick(event) {
//   setTimeout(() => {
//     this.classList.add('active');
//   }, 1000)
// }

// function loop(texto) {
//   console.log(texto);
// }
// setInterval(loop, 1000, 'Passou 1s');

// // loop a cada segundo
// let i = 0;
// setInterval(() => {
//   console.log(i++);
// }, 1000);

// const contarAte10 = setInterval(callback, 1000);

// let i = 0;
// function callback() {
//   console.log(i++);
//   if (i > 10) {
//     clearInterval(contarAte10);
//   }
// }

// Mude a cor da tela para azul e depois para vermelho a cada 2s.

// setInterval(() => {
//   document.body.classList.toggle('active');
// },2000)

// Crie um cronometro utilizando o setInterval. Deve ser possível
// iniciar, pausar e resetar (duplo clique no pausar).

const btn = document.querySelectorAll("button");
const time = document.querySelector(".time");
let seconds = 0;
let timer

btn[0].onclick = () => {
  timer = setInterval(() => {
    time.innerText = seconds++
  },100)
  btn[0].disabled = true
};
btn[1].onclick = () => {
  clearInterval(timer)
  btn[0].disabled = false
};
btn[1].ondblclick = () => {
  time.innerText = 0
  seconds = 0
};
