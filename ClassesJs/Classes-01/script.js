// function Button(text, background) {
//   this.text = text;
//   this.background = background;
// }

// Button.prototype.element = function () {
//   const buttonElement = document.createElement("button");
//   buttonElement.innerText = this.text;
//   buttonElement.style.background = this.background;
//   return buttonElement;
// };

// class Button {
//   constructor(text, background, color) {
//     (this.text = text), (this.background = background),(this.color = color);
//   }
//   element() {
//     const btnElement = document.createElement("button");
//     btnElement.innerText = this.text;
//     btnElement.style.backgroundColor = this.background;
//     btnElement.style.color = this.color;
//     return btnElement;
//   }
//   appendTo (target){
//     const trgElement = document.querySelector(target);
//     trgElement.appendChild(this.element());
//   }
// }

// const blueButton = new Button("Ola", "#00f", "white");
// const redButton = new Button("Tchau", "#f00", "green");
// blueButton.appendTo('body')
// redButton.appendTo('body')
// console.log(blueButton);

// class Button {
//   constructor(options) {
//     this.options = options;
//   }
// }

// const blueOptions = {
//   backgroundColor: 'blue',
//   color: 'white',
//   text: 'Clique',
//   borderRadius: '4px',
// }

// const blueButton = new Button(blueOptions);
// blueButton.options;

// console.log(blueButton)

// * Por padrão todos os métodos criados dentro da classe irão para o protótipo da mesma. Porém podemos criar métodos diretamente na classe utilizando a palavra chave static. Assim como [].map() é um método de uma array e Array.from() é um método do construtor Array. Você pode utilizar um método static para retornar a própria classe com propriedades já pré definidas.* \\

// class Button {
//   constructor(text) {
//     this.text = text;
//   }
//   static create(background) {
//     const elementButton = document.createElement('button');
//     elementButton.style.background = background;
//     elementButton.innerText = 'Clique';
//     return elementButton;
//   }
// }

// const blueButton = Button.create('blue');

class Button {
  constructor(text, background, color) {
    (this.text = text), (this.background = background), (this.color = color);
  }
  element() {
    const btnElement = document.createElement("button");
    btnElement.innerText = this.text;
    btnElement.style.backgroundColor = this.background;
    btnElement.style.color = this.color;
    return btnElement;
  }
  appendTo(target) {
    const trgElement = document.querySelector(target);
    trgElement.appendChild(this.element());
  }
  static blueButton(text) {
    return new Button(text, "blue", "white");
  }
}

const blueButton = Button.blueButton("Clique");
blueButton.appendTo("body");
