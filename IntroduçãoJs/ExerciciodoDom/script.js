// // const animais = document.getElementById('animais')
// // console.log(animais)

// // const gridSection = document.getElementsByClassName('grid-section')

// // const primeiraLi = document.querySelector('li')
// // const primeiraUl = document.querySelector('ul')
// // const linkInternos = document.querySelector('[href^="#"]')
// // const todasLi = document.querySelectorAll('li')
// // const todasUl = document.querySelectorAll('ul')
// // const linksInternos = document.querySelectorAll('[href^="#"]')
// // const animaisImg = document.querySelectorAll('.animais img')

// // const gridSectionHTML = document.getElementsByClassName('grid-section')
// // const gridSectionNode = document.querySelectorAll('.grid-section')

// // console.log(gridSectionHTML)
// // console.log(gridSectionNode)

// // const arrayGrid = Array.from(gridSectionHTML)

// // arrayGrid.forEach(function(item, index){
// //   console.log(item)
// //   console.log(index)
// // })

// // Retorne no console todas as imagens do site
// const allImg = document.querySelectorAll('img')
// console.log(allImg)

// // Retorne no console apenas as imagens que começaram com a palavra imagem

// const imagems = document.querySelectorAll('img[src^="./img/imagem"]')
// console.log(imagems)

// // Selecione todos os links internos (onde o href começa com #)

// const linksInternos = document.querySelectorAll('[href^="#"]')
// console.log(linksInternos)

// // Selecione o primeiro h2 dentro de .animais-descricao

// const firstH2 = document.querySelector('.animais-descricao h2')
// console.log(firstH2)

// // Selecione o último p do site

// // const ultimo = document.querySelector('body p:last-child')
// // console.log(ultimo)
// const ultimo = document.querySelectorAll('p')
// // console.log(ultimo[ultimo.length -1])
// console.log(ultimo[--ultimo.length])

// const imgs = document.querySelectorAll('img')
// const imgsArray = Array.from(imgs)
// // console.log(imgs)
// // imgsArray.forEach(function(item,index,array){
// //   console.log(item,index,array)
// // })

// // imgs.forEach((items) => {
// //   console.log(items)
// // })

// // Mostre no console cada parágrado do site
// const p = document.querySelectorAll("p");
// console.log(p);
// p.forEach((item) => console.log(item));

// // Mostre o texto dos parágrafos no console

// p.forEach((item,index) => console.log(index,item.innerText));

// // Como corrigir os erros abaixo:
// const imgs = document.querySelectorAll("img");

// imgs.forEach((item, index) => {
//   console.log(item, index);
// });

// let i = 0;
// imgs.forEach(() => console.log(i++));

// const menu = document.querySelector('.menu')

// menu.classList.add('ativo')
// menu.classList.remove('ativo')
// menu.classList.toggle('ativo')
// menu.classList.contains('ativo')
// menu.className += (' ativado')

// const animais = document.querySelector('.animais')
// console.log(animais.attributes[1])

// const imagem = document.querySelector('img')
// console.log(imagem.getAttribute('src'))
// imagem.setAttribute('alt','rapozinha doidera') /** Coloca o atributo e ou seta ele*/
// console.log(imagem.alt)
// console.log(imagem.hasAttribute('alt'))
// imagem.removeAttribute('alt')
// console.log(imagem.hasAttribute('alt'))


// Adicione a classe ativo a todos os itens do menu
const menu = document.querySelectorAll('.menu a')
menu.forEach((item) =>{
item.classList.add('ativo')
console.log(item)
})

// // Remove a classe ativo de todos os itens do menu e mantenha apenas no primeiro
menu.forEach((item) =>{
  item.classList.remove('ativo')
  console.log(item)
})

menu[0].classList.add("ativo")
console.log(menu[0])
  
// Verifique se as imagens possuem o atributo alt
const imgs = document.querySelectorAll('img')
imgs.forEach((item) =>{
  
  console.log(item.hasAttribute('alt'))
})
// Modifique o href do link externo no menu
const link = document.querySelector('[href^="http"]')
link.setAttribute('href', "https://www.google.com")

console.log(link)

