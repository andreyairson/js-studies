// Verifique a distância da primeira imagem
// em relação ao topo da página
const img = document.querySelector("img");
const distancia = img.offsetTop;
console.log(distancia);

// Retorne a soma da largura de todas as imagens
const somaImagens = () => {
  const imgs = document.querySelectorAll("img");
  let soma = 0;
  imgs.forEach((img) => (soma += img.offsetWidth));
  console.log(soma);
};

window.onload = () => {
  somaImagens();
};

// Verifique se os links da página possuem
// o mínimo recomendado para telas utilizadas
// com o dedo. (48px/48px de acordo com o google)

const links = document.querySelectorAll("a");
links.forEach((link) => {
  const linkWidth = link.offsetWidth;
  const linkHeigth = link.offsetHeight;
  if (linkHeigth > 48 && linkWidth > 48)
    console.log(link, "Possui o mínimo recomendado!", linkHeigth, linkWidth);
  else console.log(link, "Não possui o mínimo recomendado!",linkHeigth, linkWidth);
});

// Se o browser for menor que 720px,
// adicione a classe menu-mobile ao menu
const menu = document.querySelector(".menu");
const small = window.matchMedia("(max-width: 720px)");
if (small.matches) {
  menu.classList.add("menu-mobile");
  console.log(menu);
}
