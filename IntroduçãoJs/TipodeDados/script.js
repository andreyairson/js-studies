// /* Data Tips */

// /* String */
// const text = "Cursos de Javascript";
// const condition = "Novos";
// const phrase = condition + " " + text;

// console.log(phrase);

// /* Number */

// let total = 0;

// const money = 50;
// const buys = 25;
// const change = money - buys;
// const trasform = Number("40") - 60;

// console.log(change);
// console.log(trasform);

// /* Objects */

// const tolkien = {
//   name: "John Ronald Reuel Tolkien",
//   nasc: 1892,
//   death: 1973,
//   wife: "Edith Bratt",
//   greatestworks: "Hobbit and Lords of Rings",
// };

// const hobbit = {
//   name: "Hobbit",
//   created: 1937,
// };

// const lords = {
//   name: "Lords of Rings",
//   created: 1954,
// };

// console.log(tolkien);
// console.log(tolkien.greatestworks);
// console.log(
//   tolkien.name + " was the creator it " + hobbit.name + " and " + lords.name
// );
// console.log(hobbit);
// console.log(lords);

/* Numeros e operadores */

var total = 10 + 5 * 2 / 2 + 20;
console.log(total);

var soma1 = "10 Laranjas" - 5;
console.log(soma1);
var soma2 = 40 - "R$50";
console.log(soma2);

var soma3 = +"200" + 50;
console.log(soma3);

var soma4 = 5;
console.log(++soma4);

var valor = "80", 
unidade = "kg",
peso = `${valor}${unidade}`,
pesoPorDois = valor /2 + unidade

console.log(valor,unidade,peso,pesoPorDois)


