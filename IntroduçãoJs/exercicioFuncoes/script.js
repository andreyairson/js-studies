// // const profissao = "Front End";
// // function dados() {
// //   const dadosTot = {
// //     name: "Andrey",
// //     idade: 24,
// //     sexo: "Masculino",
// //   };
// //   const irmao = "Andrew";
// //   function outrosDados() {
// //     const compleDados = {
// //       endereço: "Rio de Janeiro",
// //       idade: 25,
// //     };
// //     return `${dadosTot.name}, ${irmao}, ${dadosTot.idade}, ${compleDados.endereço}, ${compleDados.idade}`;
// //   }
// //   return outrosDados();
// // }
// // console.log(dados());

// // Crie uma função para verificar se um valor é Truthy

// function truthy(verdade) {
//   if (verdade) {
//     return "este valor é truthy";
//   }
// }

// console.log(truthy(0));

// // Crie uma função matemática que retorne o perímetro de um quadrado
// // lembrando: perímetro é a soma dos quatro lados do quadrado

// function perimetroQuadrado(l1, l2, l3, l4) {
//   return l1 + l2 + l3 + l4;
// }

// console.log(perimetroQuadrado(2, 2, 2, 2));

// // Crie uma função que retorne o seu nome completo
// // ela deve possuir os parâmetros: nome e sobrenome

// function nomeCompleto(nome, sobrenome) {
//   return `${nome} ${sobrenome}`;
// }

// console.log(nomeCompleto("andrey", "borges"));

// // Crie uma função que verifica se um número é par

// function parOUimpar(number) {
//   const modulo = number % 2;

//   if (modulo === 0) {
//     console.log(number, "é par");
//   } else {
//     console.log(number, "é impar");
//   }
// }

// console.log(parOUimpar(16));

// // Crie uma função que retorne o tipo de
// // dado do argumento passado nela (typeof)
// function tipo(data) {
//   return typeof data;
// }

// console.log(tipo("alou"));

// // addEventListener é uma função nativa do JavaScript
// // o primeiro parâmetro é o evento que ocorre e o segundo o Callback
// // utilize essa função para mostrar no console o seu nome completo
// // quando o evento 'scroll' ocorrer.

// const scrollei = () => console.log("Andrey Airson");

// addEventListener("scroll", scrollei);

// // Corrija o erro abaixo
// var totalPaises = 193;
// function precisoVisitar(paisesVisitados) {
//   return `Ainda faltam ${totalPaises - paisesVisitados} países para visitar`;
// }
// function jaVisitei(paisesVisitados) {
//   return `Já visitei ${paisesVisitados} do total de ${totalPaises} países`;
// }
// console.log(precisoVisitar(20));
// console.log(jaVisitei(20));

// //

// // const quadrado = {
// //   lados: 4,
// //   area: function (ld) {
// //     return ld * ld;
// //   },
// //   perimetro: function (ld) {
// //     return this.lados * ld;
// //   },
// // };

// const quadrado = {
//   lados: 4,
//   area(ld) {
//     return ld * ld;
//   },
//   perimetro(ld) {
//     return this.lados * ld;
//   },
// }

// console.log(quadrado.perimetro(3));
// console.table(quadrado);


// const menu = {
//   width: 800,
//   height: 50,
//   backgroundColor: "#f00",
// }

// const bg = menu.backgroundColor

// /* o "THIS" sempre vai fazer referencia ao proprio objeto, ele faz um callback */
// // const aluno =  [
// //   nome = 'Guilherme',
// //   idade = 27,
// //   profissão = 'Desenvolvedor'
// // ]

// // let nomeDoAluno = () => aluno.nome = "Andrew" + ' ' + aluno[1] + ' ' + aluno[2]

// // console.log(nomeDoAluno())

// // const estacionamento = [
// //   {
// //     cliente:"vanderson",
// //     veiculo: "carro",
// //     fabricante:"BMW",
// //     anoDoCarro:2022
// //   },
// //   {
// //     cliente:"vanderson",
// //     veiculo: "moto",
// //     fabricante:"Suzuki",
// //     anoDoCarro:2022
// //   },
// //   {
// //     cliente:"guilherme",
// //     veiculo: "carro",
// //     fabricante:"Porsche",
// //     anoDoCarro:2022
// //   }
// // ]

// // console.log(estacionamento[2])






// // Crie um objeto com os seus dados pessoais
// // Deve possui pelo menos duas propriedades nome e sobrenome

// const pessoa = {
//   nome:"Andrey Airson",
//   sobrenome:"Du Caralho",
//   Idade: 24,
//   sexo: "Masculino",
//   totNome(){
//     return `${this.nome} ${this.sobrenome}`
//   }
// }

// console.log(pessoa.totNome())
// // Crie um método no objeto anterior, que mostre o seu nome completo

// // Modifique o valor da propriedade preco para 3000
// var carro = {
//   preco: 1000,
//   portas: 4,
//   marca: 'Audi',
// }

// carro.preco = 4000

// console.log(carro.preco)

// // Crie um objeto de um cachorro que represente um labrador,
// // preto com 10 anos, que late ao ver um homem

// const cachorro = {
//   nome: "Labrador",
//   pelo: "#000",
//   idade: `${10} anos.`,
//   açao (pessoa){
//     if (pessoa === "homem"){
//       return "au au"
//     }else if (pessoa === "mulher"){
//       return "lambida"
//     }else{
//       return "medo"
//     }

//   }
// }
// console.log(cachorro.açao("teucu"))


// const nomee = "andrey";
// const numero = 12.7






// // nomeie 3 propriedades ou métodos de strings

// console.log(nomee.toLocaleUpperCase())
// console.log(nomee.length)
// console.log(numero.toString())

// // nomeie 5 propriedades ou métodos de elementos do DOM

// const btn = document.querySelector(".btn")
// btn.addEventListener
// btn.innerHTML
// btn.classList.add
// btn.id
// btn.ariaLabel

// // busque na web um objeto (método) capaz de interagir com o clipboard, 
// // clipboard é a parte do seu computador que lida com o CTRL + C e CTRL + V


