// Quando o usuário clicar nos links internos do site,
// adicione a classe ativo ao item clicado e remova dos
// demais itens caso eles possuam a mesma. Previna
// o comportamento padrão desses links
const linksInternos = document.querySelectorAll("a[href^='#']");

const handleLink = (ev) => {
  ev.preventDefault()

  linksInternos.forEach((link)=>{
    link.classList.remove('ativo')
  })

  ev.target.classList.add('ativo')
  console.log(ev.target);

};

linksInternos.forEach((link) =>{
  link.addEventListener('click', handleLink)
})

// Selecione todos os elementos do site começando a partir do body,
// ao clique mostre exatamente quais elementos estão sendo clicados
const todosElementos = document.querySelectorAll('body *')

// const handleBody = (ev) =>{
//   ev.preventDefault()
//   console.log(ev.currentTarget)
// }

// todosElementos.forEach((itens) =>{
//   itens.addEventListener('click',handleBody)
//   })

// Utilizando o código anterior, ao invés de mostrar no console,
// remova o elemento que está sendo clicado, o método remove() remove um elemento
// const handleBody = (ev) =>{
//   ev.preventDefault()
//   ev.currentTarget.remove()
// }
// todosElementos.forEach((itens) =>{
//   itens.addEventListener('click',handleBody)
//   })
// Se o usuário clicar na tecla (t), aumente todo o texto do site.

const handleKeybord = (ev) =>{
  
  if (ev.key === "g"){
    document.documentElement.classList.add('textG')
  }
  if (ev.key === "p"){
    document.documentElement.classList.add('textP')
  }
  if (ev.key === "n"){
    document.documentElement.classList.remove('textP')
    document.documentElement.classList.remove('textG')
  }
  
}

window.onkeydown = handleKeybord