// const body = document.querySelector("body");
// const handleClick = () => body.classList.toggle("actived");
// window.addEventListener("click", handleClick);
// function handleClick() {
//   body.classList.toggle("actived");
// }



// var multiplicar
// Andrey - functions servem para tratar um bloco de codigos, e depois que finalizadas podem ser reutilizado varias vezes para casos parecidos.
// Sempre entendendo que a aplicabilidade dela é de acordo com o tratamento feito dentro de seu escopo, não se pode aplicar uma function que trata dados do tipo number, em uma string pois ocasionará um erro e assim sucessivamente.


// existem 2 tipos de function, as function normais e as arrowfunctions,a diferença entre elas é que uma function tem o codigo mais extenso tem que declarar seu return, enquanto as arrowfunctions tem uma escrita mais simples para deixar o codigo mais facil e optimzado.
// 	um exemplo de expressions functions seria :

// const body = document.querySelector("body")
// function handleClick(){
//   body.classList.toggle("actived")
// }
// window.addEventListener('click',handleClick)

// e arrowfunctions seria :
// const body = document.querySelector("body");
// const handleClick = () => body.classList.toggle("actived");
// window.addEventListener("click", handleClick);